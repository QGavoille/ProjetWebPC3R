<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Ginko-nnect</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css"
          integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI="
          crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js"
            integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM="
            crossorigin=""></script>
    <script src="js/leaflet.polylineoffset.js"></script>
    <script src="js/index.js"></script>
</head>
<body>
    <main>
        <!--<div id="side-box-left" class='side-box'><p></p></div>-->
        <div id="map"></div>
        <div id='side-box-right' class='side-box'><p></p></div>
    </main>
</body>
</html>
