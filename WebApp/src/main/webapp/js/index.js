
async function clickHandler(e) {
    let latlng = e.latlng;
    let line = e.target.options.className.split(" ")[0];
    let direction = e.target.options.className.split(" ")[1];

    let line_info = await fetch('http://localhost:8081/WebApp_war_exploded/lines/getOneLigne/' + line).then(response => response.json());

    let line_name = line_info.content.numLignePublic;
    let displayed_name = String(line_info.content.variantes[(direction === "going") ? 0 : 1].destination);
    let destination_add = String(line_info.content.variantes[(direction === "going") ? 0 : 1].precisionDestination);
    //Popup contains a rectangle of the line color, with the line name written in the text color on it
    L.popup().setLatLng(latlng).setContent(
        "<div class='popup'>" + "<span class='line-display'><span class='line-name' style='background-color: #" +
        line_info.content.couleurFond + "; color: #" + line_info.content.couleurTexte + ";'>" + line_name +"</span>" +
        displayed_name + "<br><span class='destination'>" + destination_add + "</span></span></div>")
        .openOn(e.target._map);
}

async function clickStopHandler(e) {
    let latlng = e.latlng;
    let stop = e.target.options.icon.options.className.split(" ")[1];

    let stop_infos = (await fetch('http://localhost:8081/WebApp_war_exploded/stops/getArret/' + stop).then(response => response.json())).content;

    // Show the stop name and the lines passing by it
    let lines = (await fetch('http://localhost:8081/WebApp_war_exploded/stops/getLinesOnStop/' + stop).then(response => response.json())).content;
    let lines_on_stop = [];
    for (let line of lines) {
        if (line.id <= 12 || line.id === "101" || line.id === "102" ) {
            lines_on_stop.push(line);
        }
    }

    let lines_on_stop_content = "";
    for (let line of lines_on_stop) {
        lines_on_stop_content += "<tr class='lines-list'><td><span class='line-name' style='background-color: #" + line.couleurFond + "; color: #" + line.couleurTexte + ";'>" + line.numLignePublic + "</span></td>";
        let next_passage = (await fetch('http://localhost:8081/WebApp_war_exploded/stops/getNextPassages/' + stop + '/' + line.id + '/1').then(response => response.json())).content[0];
        if (next_passage !== undefined) {
            lines_on_stop_content += "<td class='next-passage'>" + next_passage.temps + "</td></tr>";
        } else {
            lines_on_stop_content += "<td class='next-passage'>Pas de passage</td></tr>";
        }
    }

    L.popup().setLatLng(latlng).setContent(
        "<div class='popup stop-popup'>" + "<table><tr><td class='stop-name' colspan='2'>" + stop_infos.nom + "</td></tr>" + lines_on_stop_content + "<tr><td class='button-horaires " + stop + "' colspan='2'>Consulter les horaires</td></tr></table></div>")
        .openOn(e.target._map);

    let buttons = document.getElementsByClassName("button-horaires");
    for (let elmt of buttons) {
        elmt.addEventListener('click', clickHoraireHandler);
    }

}

async function clickHoraireHandler(e) {
    let stop_name = e.target.className.split(" ")[1];
    let stop_infos = (await fetch('http://localhost:8081/WebApp_war_exploded/stops/getArret/' + stop_name).then(response => response.json())).content;
    let lines = (await fetch('http://localhost:8081/WebApp_war_exploded/stops/getLinesOnStop/' + stop_name).then(response => response.json())).content;

    let side_box = document.getElementById("side-box-right");

    side_box.style.visibility = "visible";

    let lines_on_stop = [];
    for (let line of lines) {
        if (line.id <= 12 || line.id === "101" || line.id === "102" ) {
            lines_on_stop.push(line);
        }
    }

    // One line for each line passing by the stop
    let lines_on_stop_content = "";
    for (let line of lines_on_stop) {
        lines_on_stop_content += "<tr class='lines-list'><td><span class='line-name' style='background-color: #" + line.couleurFond + "; color: #" + line.couleurTexte + ";'>" + line.numLignePublic + "</span></td>";
        let next_passage = (await fetch('http://localhost:8081/WebApp_war_exploded/stops/getNextPassages/' + stop_name + '/' + line.id + '/3').then(response => response.json())).content;
        if (next_passage.length === 0) {
            lines_on_stop_content += "<td class='next-passage'>Pas de passage</td></tr>";
            continue;
        }
        for (let passage in next_passage) {
            if (passage >= 3) break;
            lines_on_stop_content += "<td class='next-passage'>" + next_passage[passage].temps + "</td>";
        }
    }

    // Commentaires
    let aff_commentaire = async () => {
        let commentaires = (await fetch('http://localhost:8081/WebApp_war_exploded/stops/getCommentaires/' + stop_name).then(response => response.json())).content;
        let commentaires_content = "";
        if (commentaires.length === 0) {
            commentaires_content = "<tr class='commentaire'><td colspan='5'>Pas de commentaires</td></tr>";
        } else {
            for (let commentaire of commentaires) {
                //remove the first and last character of the author and the comment string (they are quotes)
                let author = commentaire.author;
                let comment = commentaire.comment;
                commentaires_content += "<tr class='commentaire'><td colspan='5'><span class='author-comment'>" + author + "</span> : " + comment + "</td></tr>";
            }
        }

        //input string for comment
        commentaires_content += "<tr class='commentaire'><td colspan='5'><input type='text' id='author-input' placeholder='Votre pseudo'><input type='text' id='comment-input' placeholder='Votre commentaire'><span id='btn-commentaire'>Envoyer</span></td></tr>";

        side_box.children[0].innerHTML = "<table id='table-horaire'><tr><td class='stop-name' colspan='5'>" + stop_infos.nom + "</td><tr><td></td><td>Prochain</td><td>Suivant</td><td>Troisième</td></tr>" + lines_on_stop_content + "<tr ><td class='stop-name' colspan='5' style:'margin-top: 0.5em'>Commentaires</td></tr>" + commentaires_content + "</table>";

        document.getElementById('btn-commentaire').addEventListener('click', async () => {
            let comment = document.getElementById('comment-input').value;
            let author = document.getElementById('author-input').value;
            if (comment === "") {
                alert("Veuillez entrer un commentaire");
                return;
            }
            if (author === "") {
                alert("Veuillez entrer un pseudo");
                return;
            }
            await fetch('http://localhost:8081/WebApp_war_exploded/stops/putCommentaire/' + stop_name + '?author=' + author + '&comment=' + comment, {
                method: 'PUT'
            }).then(response => response.json());
            console.log("commentaire ajouté")
            await aff_commentaire();
        });
    };

    await aff_commentaire();
}

/*async function createLeftBox() {
    let side_box = document.getElementById("side-box-left");

}*/

async function createStopMarker(stop) {
    let lines_on_stop = (await fetch('http://localhost:8081/WebApp_war_exploded/stops/getLinesOnStop/' + stop.id).then(response => response.json())).content;
    let urban_lines_on_stop = [];
    let markers = [];
    for (let line of lines_on_stop) {
        if (line.id <= 12 || line.id === "101" || line.id === "102" ) {
            urban_lines_on_stop.push(line);
        }
    }

    let nb_lines_on_stop = urban_lines_on_stop.length;
    // create one circle marker for each line passing by the stop, display it in a defined way depending on the number of lines in order to avoid overlapping
    let rectangle = [];

    for (let stop of urban_lines_on_stop) {
        rectangle.push("<td class='stop-rectangle' style='background-color: #" + stop.couleurFond + "; color: "+ stop.couleurFond +";'>A</td>");
    }

    let marker_content;
    switch (nb_lines_on_stop) {
        case 0:
            marker_content = "";
            break;
        case 1:
            marker_content = "<table class='stops'><tr>" + rectangle[0] + "</tr></table>";
            break;
        case 2:
            marker_content = "<table class='stops'><tr>" + rectangle[0] + rectangle[1] + "</tr></table>";
            break;
        case 3:
            marker_content = "<table class='stops'><tr>" + rectangle[0] + rectangle[1] + "</tr><tr>" + rectangle[2] + "</tr></table>";
            break;
        case 4:
            marker_content = "<table class='stops'><tr>" + rectangle[0] + rectangle[1] + "<</tr><tr>" + rectangle[2] + rectangle[3] + "</tr></table>";
            break;
        case 5:
            marker_content = "<table class='stops'><tr>" + rectangle[0] + rectangle[1] + "</tr><tr>" + rectangle[2] + rectangle[3] + rectangle[4] + "</tr></table>";
            break;
        case 6:
            marker_content = "<table class='stops'><tr>" + rectangle[0] + rectangle[1] + rectangle[2] + "</tr><tr>" + rectangle[3] + rectangle[4] + rectangle[5] + "</tr></table>";
            break;
        case 7:
            marker_content = "<table class='stops'><tr>" + rectangle[0] + rectangle[1] + rectangle[2] + "</tr><tr>" + rectangle[3] + rectangle[4] + rectangle[5] + rectangle[6] + "</tr></table>";
            break;
        case 8:
            marker_content = "<table class='stops'><tr>" + rectangle[0] + rectangle[1] + rectangle[2] + rectangle[3] + "</tr><tr>" + rectangle[4] + rectangle[5] + rectangle[6] + rectangle[7] + "</tr></table>";
            break;
        default:
            throw new Error("Too many lines on stop");
    }

    return L.marker([stop.latitude, stop.longitude], {icon : L.divIcon({ className: 'stop-marker ' + stop.id, html: marker_content, iconSize: [30, 30] })});
}
document.addEventListener('DOMContentLoaded', async function() {
    const map = L.map('map').setView([47.23650045014186, 6.021690430133319], 14);
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> | Data from <a href="https://api.ginko.voyage">Ginko API</a>'
    }).addTo(map);

    const lines = (await fetch('http://localhost:8081/WebApp_war_exploded/lines/getAllLignes').then(response => response.json())).content;
    let linepaths = {};
    for (let line in lines) {
        linepaths[lines[line].id] = {going: [], return: []};
        const line_path = (await fetch('http://localhost:8081/WebApp_war_exploded/lines/getOneLigne/' + lines[line].id).then(response => response.json())).content.path;
        line_path.going.forEach(point => linepaths[lines[line].id].going[point.order] = [point.lat, point.lng]);
        line_path.returning.forEach(point => linepaths[lines[line].id].return[point.order] = [point.lat, point.lng]);
    }
    for(let line in lines) {
        let line1 = L.polyline(linepaths[lines[line].id].going, {color: "#"+lines[line].couleurFond, className : lines[line].id+ " " + (lines[line].variantes[0].sensAller ? "going" : "return") + " " + "line", smoothFactor: 10.0, weight: 5});
        let line2 = L.polyline(linepaths[lines[line].id].return, {color: "#"+lines[line].couleurFond, className : lines[line].id+ " " + (lines[line].variantes[1].sensAller ? "going" : "return") + " " + "line", smoothFactor: 10.0, weight: 5});

        line1.on('click', clickHandler);
        line2.on('click', clickHandler);

        line1.addTo(map);
        line2.addTo(map);
    }

    const stops = (await fetch('http://localhost:8081/WebApp_war_exploded/stops/getArrets').then(response => response.json())).content;

    for (let stop of stops) {
        let marker = await createStopMarker(stop);
        marker.on('click', clickStopHandler);
        marker.addTo(map);
        console.log(stop);
    }

    //await createLeftBox();

});