package fr.qgavoille.app.startup;

import com.google.gson.Gson;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import org.mariadb.jdbc.Connection;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;

/*
 * Utility class to handle startup process
 */
@WebListener
public class StartupProcess implements ServletContextListener {

  @Override
  public void contextInitialized(ServletContextEvent sce) {
    ServletContext context = sce.getServletContext();

    System.out.println("Starting up...");
    System.out.println("Checking config.json file...");

    InputStreamReader config_reader;
    try {
      InputStream config_file = StartupProcess.class.getResourceAsStream("/config.json");
      assert config_file != null;
      config_reader = new InputStreamReader(config_file, StandardCharsets.UTF_8);
    } catch (Exception e) {
      System.out.println("Missing config.json file, aborting startup");
      throw new RuntimeException(e);
    }

    HashMap config;
    try {
      Gson gson = new Gson();
      config = gson.fromJson(config_reader, HashMap.class);
    } catch (Exception e) {
      System.out.println("Invalid config.json file, aborting startup");
      throw new RuntimeException(e);
    }


    if (config.get("DB_ADDRESS") == null || config.get("DB_PORT") == null || config.get("DB_DATABASE") == null || config.get("DB_USERNAME") == null || config.get("DB_PASSWORD") == null || config.get("API_KEY") == null || config.get("API_URL") == null) {
      System.out.println("Missing config.json file, aborting startup");
      throw new RuntimeException("Missing config.json file");
    }
    System.out.println("config.json file found");

    context.setAttribute("api-key", config.get("API_KEY"));
    context.setAttribute("api-url", config.get("API_URL"));

    System.out.println("Checking database connection...");
    try {
      // Retrieve connection from config.json file
      String dbAddress = (String) config.get("DB_ADDRESS");
      String dbPort = (String) config.get("DB_PORT");
      String dbDatabase = (String) config.get("DB_DATABASE");
      String dbUsername = (String) config.get("DB_USERNAME");
      String dbPassword = (String) config.get("DB_PASSWORD");

      System.out.println("Connecting to database...");

      DriverManager.registerDriver(new org.mariadb.jdbc.Driver());
      Connection connection = (Connection) DriverManager.getConnection("jdbc:mariadb://" + dbAddress + ":" + dbPort + "/" + dbDatabase, dbUsername, dbPassword);
      context.setAttribute("dbConnection", connection);
      System.out.println("Database connection successful");
    } catch (SQLException e) {
      System.out.println("Database connection failed, aborting startup");
      System.out.println(e.getMessage());
      throw new RuntimeException(e);
    }

    System.out.println("Startup successful");

  }

  @Override
  public void contextDestroyed(ServletContextEvent sce) {
    // Close database connection
    ServletContext context = sce.getServletContext();
    Connection connection = (Connection) context.getAttribute("dbConnection");
    try {
      connection.close();
    } catch (SQLException e) {
      System.out.println("Failed to close database connection");
      System.out.println(e.getMessage());
    }
  }
}
