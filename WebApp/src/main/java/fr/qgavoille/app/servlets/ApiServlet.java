package fr.qgavoille.app.servlets;

import jakarta.servlet.http.HttpServlet;

/*
 * Utility class to handle unified json responses with subclasses
 */
public abstract class ApiServlet extends HttpServlet {

  protected static abstract class GenericResponse {
    public boolean ok;
  }
  protected static class ErrorResponse extends GenericResponse {
    public String msg;

    public ErrorResponse(String msg) {
      this.ok = false;
      this.msg = msg;
    }
  }

  protected static class SuccessResponse extends GenericResponse {
    public Object content;

    public SuccessResponse(Object content) {
      this.ok = true;
      this.content = content;
    }
  }
}
