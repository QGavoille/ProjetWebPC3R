package fr.qgavoille.app.servlets;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import fr.qgavoille.app.api.CommentsHandler;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.mariadb.jdbc.Connection;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;

@WebServlet("/vehicles/*")
public class VehicleServlet extends ApiServlet {

  @Override
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String pathInfo = req.getPathInfo();
    Gson gson = new Gson();
    GenericResponse response;
    final String API_KEY = getServletContext().getAttribute("api-key").toString();
    final String API_URL = getServletContext().getAttribute("api-url").toString();
    if (pathInfo == null || pathInfo.equals("/")) {
      resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      resp.getWriter().println("Bad request");
    } else {
      String[] pathParts = pathInfo.split("/");
      switch (pathParts[1]) {
        case "getVehicule":
          HttpResponse<String> res = null;
          try {

            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create("https://"+API_URL+"/DR/getDetailsVehicule.do?apiKey="+API_KEY+"&id="+pathParts[2]))
                    .build();
            try {
              res = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            } catch (Exception e) {
              e.printStackTrace();
            }
            assert res != null;
            if (res.statusCode() != 200) throw new RuntimeException("Failed : HTTP error code : " + res.statusCode());
            LinkedTreeMap<?, ?> vehicule = (LinkedTreeMap<?, ?>) gson.fromJson(res.body(), Map.class).get("objets");

            response = new SuccessResponse(vehicule);
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_OK);
          } catch (IndexOutOfBoundsException e) {
            response = new ErrorResponse("Missing id");
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
          }
          break;
          // Affluence and position are available only with special rights
        /*case "getAffluence":
          try {
            int id = Integer.parseInt(pathParts[2]);
            response = new SuccessResponse("Affluence of vehicle " + id);
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_OK);
          } catch (IndexOutOfBoundsException e) {
            response = new ErrorResponse("Missing id");
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
          }
          break;
        case "getPosition":
          try {
            int id = Integer.parseInt(pathParts[2]);
            response = new SuccessResponse("Position of vehicle " + id);
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_OK);
          } catch (IndexOutOfBoundsException e) {
            response = new ErrorResponse("Missing informations");
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
          }
          break;*/
        case "getCommentaires":
          try {
            CommentsHandler.DB_CONNECTION = (Connection) getServletContext().getAttribute("dbConnection");
            String id = pathParts[2];
            response = new SuccessResponse(CommentsHandler.getComments(CommentsHandler.CommentType.VEHICLE, id));
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_OK);
          } catch (IndexOutOfBoundsException e) {
            response = new ErrorResponse("Missing informations");
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
          }
          break;
      }
    }
  }

  @Override
  public void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String pathInfo = req.getPathInfo();
    Gson gson = new Gson();
    GenericResponse response;
    CommentsHandler.DB_CONNECTION = (org.mariadb.jdbc.Connection) getServletContext().getAttribute("db-connection");
    if (pathInfo == null || pathInfo.equals("/")) {
      resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      resp.getWriter().println("Bad request");
    } else {
      String[] pathParts = pathInfo.split("/");
      if (pathParts[1].equals("setCommentaire")) {
        try {
          String id = pathParts[2];
          String comment = req.getParameter("comment");
          String author = req.getParameter("author");

          CommentsHandler.addComment(author, comment, CommentsHandler.CommentType.VEHICLE, id, null);

          response = new SuccessResponse("Comment added");
          gson.toJson(response, resp.getWriter());
          resp.setStatus(HttpServletResponse.SC_OK);
        } catch (IndexOutOfBoundsException e) {
          response = new ErrorResponse("Missing informations");
          gson.toJson(response, resp.getWriter());
          resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
      }
    }
  }
}
