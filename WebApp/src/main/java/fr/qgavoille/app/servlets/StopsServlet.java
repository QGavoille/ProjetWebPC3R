package fr.qgavoille.app.servlets;

import com.google.gson.Gson;
import fr.qgavoille.app.api.CommentsHandler;
import fr.qgavoille.app.api.LinesHandler;
import fr.qgavoille.app.api.StopsHandler;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.mariadb.jdbc.Connection;

import java.io.IOException;

@WebServlet("/stops/*")
public class StopsServlet extends ApiServlet {

  @Override
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String pathInfo = req.getPathInfo();
    Gson gson = new Gson();
    GenericResponse response;
    StopsHandler.API_KEY = getServletContext().getAttribute("api-key").toString();
    StopsHandler.API_URL = getServletContext().getAttribute("api-url").toString();
    StopsHandler.DB_CONNECTION = (Connection) getServletContext().getAttribute("dbConnection");
    if (pathInfo == null || pathInfo.equals("/")) {
      resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      resp.getWriter().println("Bad request");
    } else {
      String[] pathParts = pathInfo.split("/");
      switch (pathParts[1]) {
        case "getArrets":
          response = new SuccessResponse(StopsHandler.getAllStops());
          gson.toJson(response, resp.getWriter());
          resp.setStatus(HttpServletResponse.SC_OK);
          break;
      case "getArret":
          try {
            String id = pathParts[2];
            response = new SuccessResponse(StopsHandler.getOneStop(id));
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_OK);
          } catch (IndexOutOfBoundsException e) {
            response = new ErrorResponse("Missing id");
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
          }
          break;
        case "getArretLigne":
            try {
              int idLigne = Integer.parseInt(pathParts[2]);
              response = new SuccessResponse(StopsHandler.getStopsFromLine(idLigne));
              gson.toJson(response, resp.getWriter());
              resp.setStatus(HttpServletResponse.SC_OK);
            } catch (IndexOutOfBoundsException e) {
              response = new ErrorResponse("Missing informations");
              gson.toJson(response, resp.getWriter());
              resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
            break;
        case "getHoraires":
          try {
            String id = pathParts[2];
            int idLigne = Integer.parseInt(pathParts[3]);
            response = new SuccessResponse(StopsHandler.getHoraires(id, idLigne));
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_OK);
          } catch (IndexOutOfBoundsException e) {
            response = new ErrorResponse("Missing id");
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
          }
          break;
        case "getNextPassages":
          try {
            String id = pathParts[2];
            int idLigne = Integer.parseInt(pathParts[3]);
            int nb = Integer.parseInt(pathParts[4]);
            response = new SuccessResponse(StopsHandler.nextPassages(id, idLigne, nb));
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_OK);
          } catch (IndexOutOfBoundsException e) {
            response = new ErrorResponse("Missing id");
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
          }
          break;
        case "getCommentaires":
          try {
            CommentsHandler.DB_CONNECTION = (Connection) getServletContext().getAttribute("dbConnection");
            String id = pathParts[2];
            response = new SuccessResponse(CommentsHandler.getComments(CommentsHandler.CommentType.STOP, id));
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_OK);
          } catch (IndexOutOfBoundsException e) {
            response = new ErrorResponse("Missing id");
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
          }
          break;
        case "getLinesOnStop":
          try {
            String id = pathParts[2];
            response = new SuccessResponse(StopsHandler.getAllLinesOnStop(id));
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_OK);
          } catch (IndexOutOfBoundsException e) {
            response = new ErrorResponse("Missing id");
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
          }
          break;
        default:
          response = new ErrorResponse("Unknown method");
          gson.toJson(response, resp.getWriter());
          resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      }
    }
  }

  @Override
  public void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String pathInfo = req.getPathInfo();
    CommentsHandler.DB_CONNECTION = (Connection) getServletContext().getAttribute("dbConnection");
    Gson gson = new Gson();
    GenericResponse response;
    if (pathInfo == null || pathInfo.equals("/")) {
      resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      resp.getWriter().println("Bad request");
    } else {
      String[] pathParts = pathInfo.split("/");
      if (pathParts[1].equals("putCommentaire")) {
        try {
          String id = pathParts[2];
          String comment = req.getParameter("comment");
          String author = req.getParameter("author");

          CommentsHandler.addComment(author, comment, CommentsHandler.CommentType.STOP, id, null);

          response = new SuccessResponse("Comment added");
          gson.toJson(response, resp.getWriter());
          resp.setStatus(HttpServletResponse.SC_OK);
        } catch (IndexOutOfBoundsException e) {
          response = new ErrorResponse("Missing id");
          gson.toJson(response, resp.getWriter());
          resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
      } else {
        response = new ErrorResponse("Unknown method");
        gson.toJson(response, resp.getWriter());
        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      }
    }
  }
}
