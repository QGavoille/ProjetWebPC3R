package fr.qgavoille.app.servlets;

import com.google.gson.Gson;
import fr.qgavoille.app.api.CommentsHandler;
import fr.qgavoille.app.api.LinesHandler;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.mariadb.jdbc.Connection;

import java.io.IOException;

@WebServlet("/lines/*")
public class LinesServlet extends ApiServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String pathInfo = req.getPathInfo();
    Gson gson = new Gson();
    GenericResponse response;
    LinesHandler.API_KEY = getServletContext().getAttribute("api-key").toString();
    LinesHandler.API_URL = getServletContext().getAttribute("api-url").toString();
    LinesHandler.DB_CONNECTION = (Connection) getServletContext().getAttribute("dbConnection");
    if (pathInfo == null || pathInfo.equals("/")) {
      resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      resp.getWriter().println("Bad request");
    } else {
      String[] pathParts = pathInfo.split("/");
      switch (pathParts[1]) {
        case "getAllLignes":
          response = new SuccessResponse(LinesHandler.getAllLines());
          gson.toJson(response, resp.getWriter());
          resp.setStatus(HttpServletResponse.SC_OK);
          break;
        case "getOneLigne":
          try {
            int id = Integer.parseInt(pathParts[2]);
            response = new SuccessResponse(LinesHandler.getOneLineWithPath(id));
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_OK);
          } catch (IndexOutOfBoundsException e) {
            response = new ErrorResponse("Missing id");
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
          }
          break;
        case "getCommentaires":
          try {
            CommentsHandler.DB_CONNECTION = (Connection) getServletContext().getAttribute("dbConnection");
            String id = pathParts[2];
            response = new SuccessResponse(CommentsHandler.getComments(CommentsHandler.CommentType.STOP, id));
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_OK);
          } catch (IndexOutOfBoundsException e) {
            response = new ErrorResponse("Missing id");
            gson.toJson(response, resp.getWriter());
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
          }
          break;
        default:
          response = new ErrorResponse("Unknown method");
          gson.toJson(response, resp.getWriter());
          resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      }
    }
  }

  @Override
  protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String pathInfo = req.getPathInfo();
    CommentsHandler.DB_CONNECTION = (Connection) getServletContext().getAttribute("dbConnection");
    Gson gson = new Gson();
    GenericResponse response;
    if (pathInfo == null || pathInfo.equals("/")) {
      resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      response = new ErrorResponse("Bad request");
      gson.toJson(response, resp.getWriter());
    } else {
      String[] pathParts = pathInfo.split("/");
      if (pathParts[1] != "putCommentaire") {
        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        response = new ErrorResponse("Unknown method");
        gson.toJson(response, resp.getWriter());
      }
      String id = pathParts[2];
      String comment = req.getParameter("comment");
      String author = req.getParameter("author");

      CommentsHandler.addComment(author, comment, CommentsHandler.CommentType.LINE, id, null);

      response = new SuccessResponse("Comment added");
      gson.toJson(response, resp.getWriter());
    }
  }
}
