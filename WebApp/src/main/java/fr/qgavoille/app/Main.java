package fr.qgavoille.app;

import fr.qgavoille.app.api.LinesHandler;
import fr.qgavoille.app.api.StopsHandler;
import org.mariadb.jdbc.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;


public class Main {

  public static void main(String[] args) throws SQLException {
    StopsHandler.API_KEY = "8f6831fe594b40eeb0b9c7023818b54a";
    StopsHandler.API_URL = "api.ginko.voyage";
    StopsHandler.DB_CONNECTION = (Connection) DriverManager.getConnection("jdbc:mariadb://localhost:3306/pc3r_db", "ellipsialp", "admin");
    System.out.println(StopsHandler.getAllLinesOnStop("POSTE1"));
  }
}
