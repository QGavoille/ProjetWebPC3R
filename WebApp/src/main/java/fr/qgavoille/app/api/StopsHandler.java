package fr.qgavoille.app.api;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.mariadb.jdbc.Connection;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;

public class StopsHandler {
  public static String API_KEY;
  public static String API_URL;
  public static Connection DB_CONNECTION;

  public static ArrayList<Object> getAllStops() {
    LinesHandler.API_KEY = API_KEY;
    LinesHandler.API_URL = API_URL;

    Gson gson = new Gson();
    HashSet<Object> stops_ok = new HashSet<>();

    ArrayList<Object> lines = LinesHandler.getAllLines();
    for(Object line : lines) {
      LinkedTreeMap<?, ?> line_ = (LinkedTreeMap<?, ?>) line;

      ArrayList<LinkedTreeMap<?, ?>> variantes = (ArrayList<LinkedTreeMap<?, ?>>) line_.get("variantes");

      for (LinkedTreeMap<?, ?> variante : variantes) {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://"+API_URL+"/DR/getDetailsVariante.do?apiKey="+API_KEY+"&idLigne="+line_.get("id")+"&idVariante="+variante.get("id")))
                .method("POST", HttpRequest.BodyPublishers.noBody())
                .build();
        HttpResponse<String> response = null;
        try {
          response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        } catch (Exception e) {
          e.printStackTrace();
        }
        assert response != null;
        if (response.statusCode() != 200) throw new RuntimeException("Failed : HTTP error code : " + response.statusCode());
        ArrayList<?> stops = (ArrayList<?>) gson.fromJson(response.body(), Map.class).get("objets");
        stops_ok.addAll(stops);
      }

    }
    return new ArrayList<>(stops_ok);
  }

  public static Object getOneStop(String id) {
    Gson gson = new Gson();
    HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create("https://"+API_URL+"/DR/getDetailsArret.do?apiKey="+API_KEY+"&id="+id))
            .method("POST", HttpRequest.BodyPublishers.noBody())
            .build();
    HttpResponse<String> response = null;
    try {
      response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
    } catch (Exception e) {
      e.printStackTrace();
    }
    assert response != null;
    if (response.statusCode() != 200) throw new RuntimeException("Failed : HTTP error code : " + response.statusCode());
    return gson.fromJson(response.body(), Map.class).get("objets");
  }

  public static ArrayList<ArrayList<Object>> getStopsFromLine(int id) {
    LinesHandler.DB_CONNECTION = DB_CONNECTION;
    Object line = LinesHandler.getOneLineWithPath(id);
    Gson gson = new Gson();
    ArrayList<ArrayList<Object>> stops_on_line = new ArrayList<>();

    ArrayList<LinkedTreeMap<?, ?>> variantes = (ArrayList<LinkedTreeMap<?, ?>>) ((LinkedTreeMap<?, ?>) line).get("variantes");

    for (LinkedTreeMap<?, ?> variante : variantes) {
      HttpRequest request = HttpRequest.newBuilder()
              .uri(URI.create("https://"+API_URL+"/DR/getDetailsVariante.do?apiKey="+API_KEY+"&idLigne="+id+"&idVariante="+variante.get("id")))
              .method("POST", HttpRequest.BodyPublishers.noBody())
              .build();
      HttpResponse<String> response = null;
      try {
        response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
      } catch (Exception e) {
        e.printStackTrace();
      }
      assert response != null;
      if (response.statusCode() != 200) throw new RuntimeException("Failed : HTTP error code : " + response.statusCode());
      ArrayList<?> stops = (ArrayList<?>) gson.fromJson(response.body(), Map.class).get("objets");
      stops_on_line.add((ArrayList<Object>) stops);
    }
    return stops_on_line;
  }

  public static ArrayList<LinkedTreeMap<?, ?>> getAllLinesOnStop(String id) {
    id = id.replaceAll(" ", "%20");
    HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create("https://"+API_URL+"/DR/getVariantesDesservantArret.do?apiKey="+API_KEY+"&idArret="+id))
            .method("POST", HttpRequest.BodyPublishers.noBody())
            .build();
    HttpResponse<String> response = null;
    try {
      response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
    } catch (Exception e) {
      e.printStackTrace();
    }
    assert response != null;
    if (response.statusCode() != 200) throw new RuntimeException("Failed : HTTP error code : " + response.statusCode());
    Gson gson = new Gson();
    return (ArrayList<LinkedTreeMap<?, ?>>) gson.fromJson(response.body(), Map.class).get("objets");
  }

  public static ArrayList<String> getHoraires(String stop_id, int line_id) {
    ArrayList<String> horaires = new ArrayList<>();
    try {
      PreparedStatement statement = DB_CONNECTION.prepareStatement("SELECT DISTINCT arrival_time " +
              "FROM stop_times WHERE stop_id = '"+ stop_id +
              "' AND trip_id IN (SELECT trip_id FROM trips WHERE route_id = "+ line_id +") ORDER BY arrival_time ASC;");
      ResultSet result = statement.executeQuery();
      while (result.next()) {
        horaires.add(result.getString("arrival_time"));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return horaires;
  }

  public static ArrayList<Object> nextPassages(String id_stop, int id_line, int nb) {
    Gson gson = new Gson();
    ArrayList<Object> passages_ret = new ArrayList<>();
    HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create("https://"+API_URL+"/TR/getTempsLieu.do?apiKey="+API_KEY+"&idArret="+id_stop+"&nb="+nb))
            .method("POST", HttpRequest.BodyPublishers.noBody())
            .build();
    HttpResponse<String> response = null;
    try {
      response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
    } catch (Exception e) {
      e.printStackTrace();
    }
    assert response != null;
    if (response.statusCode() != 200) throw new RuntimeException("Failed : HTTP error code : " + response.statusCode());
    LinkedTreeMap<?, ?> passages = (LinkedTreeMap<?, ?>) gson.fromJson(response.body(), Map.class).get("objets");
    ArrayList<?> next_passages = (ArrayList<?>) passages.get("listeTemps");
    for(Object passage : next_passages) {
      LinkedTreeMap<?, ?> passage_ = (LinkedTreeMap<?, ?>) passage;
      if (passage_.get("idLigne").equals(Integer.toString(id_line))) {
        passages_ret.add(passage);
      }
    }
    return passages_ret;
  }
}
