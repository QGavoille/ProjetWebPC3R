package fr.qgavoille.app.api;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.mariadb.jdbc.Connection;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
 * Class to handle all external api calls related to lines
 */
public class LinesHandler {

  public static String API_KEY;
  public static String API_URL;
  public static Connection DB_CONNECTION;
  private static final String[][] TRIPS = {{"S4187J12V8334522", "S4187J12V8334449"}, {"S4187J12V8334611", "S4187J12V8334661"}, {"S4198J12V8336864", "S4192J127V8334140"}, {"S4192J127V8334187", "S4192J127V8334177"}, {"S4189J2V8330668", "S4189J2V8330673"}, {"S4189J2V8330786", "S4189J2V8330797"}, {"S4184J124V8300627", "S4184J124V8300637"}, {"S4198J124V8233878", "S4198J124V8233881"}, {"S4198J124V8233981", "S4198J124V8233986"}, {"S4198J124V8233536", "S4198J124V8233537"}, {"S4189J2V8228775", "S4189J2V8228776"}, {"S4189J2V8225671", "S4198J124V8233627"}}; // 12 lines, 2 trips per line (going and returning)

  public static ArrayList<Object> getAllLines() throws RuntimeException {
    // Returns the list of currently working urban lines

    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create("https://"+API_URL+"/DR/getLignes.do?apiKey="+API_KEY))
            .method("POST", HttpRequest.BodyPublishers.noBody()).build();
    HttpResponse<String> response = null;
    try {
      response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
    } catch (Exception e) {
      e.printStackTrace();
    }
    assert response != null;
    if (response.statusCode() != 200) throw new RuntimeException("Failed : HTTP error code : " + response.statusCode());
    Gson gson = new Gson();
    ArrayList<?> lines = (ArrayList<?>) gson.fromJson(response.body(), Map.class).get("objets");
    ArrayList<Object> lines_ok = new ArrayList<>();
    for (Object line : lines) {
      LinkedTreeMap<?,?> line_ok = (LinkedTreeMap<?, ?>) line;
      int line_id = Integer.parseInt((String) line_ok.get("id"));
      if (line_id <= 12 || line_id == 101 || line_id == 102) {
        lines_ok.add(line);
      }
    }
    return lines_ok;
  }

  public static Object getOneLineWithPath(int id) throws RuntimeException {
    ArrayList<Object> allLines = getAllLines();
    LinkedTreeMap<Object,Object> line_ok = null;
    for (Object line : allLines) {
      LinkedTreeMap<?,?> line_ = (LinkedTreeMap<?, ?>) line;
      int line_id = Integer.parseInt((String) line_.get("id"));
      if (line_id == id) {
        line_ok = (LinkedTreeMap<Object, Object>) line;
      }
    }
    if (line_ok == null) throw new RuntimeException("Line not found");
    HashMap<String, ArrayList<PathPoint>> path = getPath(id);
    line_ok.put("path", path);
    return line_ok;
  }

  private static HashMap<String, ArrayList<PathPoint>> getPath(int id) {
    try {
      int indexForTrip = (id==101 || id==102) ? id-101 : id-1;
      HashMap<String, ArrayList<PathPoint>> paths = new HashMap<>();
      PreparedStatement[] statements = new PreparedStatement[2];
      for (int i = 0; i < 2; i++) {
        statements[i] = DB_CONNECTION.prepareStatement(
                "SELECT shape_pt_lat, shape_pt_lon, shape_pt_sequence, broute_id, shapes.shape_id " +
                        "FROM shapes INNER JOIN (trips INNER JOIN routes on\n" +
                        " route_id = broute_id) on shapes.shape_id = trips.shape_id WHERE broute_id = "+ id +" AND trip_id = '"+ TRIPS[indexForTrip][i] +"';");
        ResultSet result = statements[i].executeQuery();
        ArrayList<PathPoint> path = new ArrayList<>();
        while (result.next()) {
          path.add(new PathPoint(result.getDouble("shape_pt_lat"), result.getDouble("shape_pt_lon"), result.getInt("shape_pt_sequence")));
        }
        paths.put(i==0 ? "going" : "returning", path);
      }
      return paths;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  private static class PathPoint {
    public double lat;
    public double lng;
    public int order;
    public PathPoint(double lat, double lng, int order) {
      this.lat = lat;
      this.lng = lng;
      this.order = order;
    }
  }
}
