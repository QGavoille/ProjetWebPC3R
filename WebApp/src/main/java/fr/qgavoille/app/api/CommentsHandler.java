package fr.qgavoille.app.api;

import org.mariadb.jdbc.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CommentsHandler {

  public static Connection DB_CONNECTION;

  public enum CommentType {
    STOP,
    LINE,
    VEHICLE
  }

  public static class CommentStructure {
    public String author;
    public String comment;
  }

  public static void addComment(String author, String comment, CommentType type, String id, String TTL) {
    switch (type) {
      case STOP:
        try {
          PreparedStatement statement = DB_CONNECTION.prepareStatement("INSERT INTO commentaires (expediteur, message, id_arret, ttl) VALUES (?, ?, ?, ?)");
          statement.setString(1, author);
          statement.setString(2, comment);
          statement.setString(3, id);
          statement.setString(4, TTL);
          statement.executeUpdate();
        } catch (Exception e) {
          e.printStackTrace();
        }
        break;
      case LINE:
        try {
          PreparedStatement statement = DB_CONNECTION.prepareStatement("INSERT INTO commentaires (expediteur, message, id_ligne, ttl) VALUES (?, ?, ?, ?)");
          statement.setString(1, author);
          statement.setString(2, comment);
          statement.setString(3, id);
          statement.setString(4, TTL);
          statement.executeUpdate();
        } catch (Exception e) {
          e.printStackTrace();
        }
        break;
      case VEHICLE:
        try {
          PreparedStatement statement = DB_CONNECTION.prepareStatement("INSERT INTO commentaires (expediteur, message, id_vehicule, ttl) VALUES (?, ?, ?, ?)");
          statement.setString(1, author);
          statement.setString(2, comment);
          statement.setString(3, id);
          statement.setString(4, TTL);
          statement.executeUpdate();
        } catch (Exception e) {
          e.printStackTrace();
        }
        break;
    }
  }

  public static void addComment(String author, String comment, CommentType type, String id) {
    addComment(author, comment, type, id, null);
  }

  public static ArrayList<CommentStructure> getComments(CommentType type, String id) {
    ArrayList<CommentStructure> comments = new ArrayList<>();
    String to_fetch = "";
    switch (type) {
      case STOP:
        to_fetch = "id_arret";
        break;
      case LINE:
        to_fetch = "id_ligne";
        break;
      case VEHICLE:
        to_fetch = "id_vehicule";
        break;
    }
    try {
      PreparedStatement statement = DB_CONNECTION.prepareStatement("SELECT * FROM commentaires WHERE "+to_fetch+" = ?");
      statement.setString(1, id);
      ResultSet result = statement.executeQuery();
      while (result.next()) {
        CommentStructure comment = new CommentStructure();
        comment.author = result.getString("expediteur");
        comment.comment = result.getString("message");
        comments.add(comment);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return comments;
  }
}
