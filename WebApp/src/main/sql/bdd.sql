CREATE TABLE IF NOT EXISTS commentaires
(
    id            INT(11)      AUTO_INCREMENT NOT NULL,
    expediteur    VARCHAR(255) NOT NULL,
    message       TEXT         NOT NULL,
    TTL           DATETIME     NULL,
    id_ligne      VARCHAR(255) NULL,
    id_arret      VARCHAR(255) NULL,
    id_vehicule   VARCHAR(255) NULL,
    PRIMARY KEY (id)
);