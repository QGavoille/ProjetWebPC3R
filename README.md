# Projet Web PC3R : Ginko-nnect
## Par Quentin GAVOILLE


## Utilisation

- Base de données : [MariaDB](https://mariadb.org/)
- Serveur : [Apache Tomcat 10.1](https://tomcat.apache.org/)
- Langage : [Java JDK 17](https://www.java.com/fr/)
- Maven : [Maven 3.8.3](https://maven.apache.org/)

Le serveur est développé pour être déployé sur le port 8081 de la machine, à l'adresse `http://localhost:8081/WebApp_war_exploded/`

La configuration des identifiants pour la base de données se fait dans le fichier `src/main/resources/config.json` (le modèle est dans `src/main/resources/config-minimal.json`)

Ce fichier contient également ma clé API personnelle utilisée pour me connecter à l'API de [Ginko](https://api.ginko.voyage/), bien qu'il s'agisse théoriquement d'une information confidentielle.

Il faut également importer le fichier GTFS (`src/resources`) dans la base de données. Pour cela, et éxécuter le script dans `src/sql`.

Ensuite, l'application s'éxécute comme un projet Maven classique.


## Présentation du projet
### Introduction

Ce projet est une application web réalisée dans le cadre de l'UE PC3R du M1 STL de Sorbonne Université.
Le sujet d'implémentations était libre à quelques contraintes près :
- Serveur basé sur Tomcat (ou sur le package go net/http) sans utilisation de framework.
- Utilisation d'une API externe.
- Implémentation d'une composante sociale (interaction entre utilisateurs).
- Requêtes asynchrones (AJAX) et utilisation de JSON.

### Sujet choisi

Le sujet choisi est une application web permettant de consulter les horaires des transports en commun
de l'agglomération de Besançon (Ginko) et de les partager avec d'autres utilisateurs.
Le choix de le faire avec la ville de Besançon s'est fait pour plusieurs raisons :
- C'est la "grande ville" la plus proche de mon domicile, j'y ai obtenu ma licence et je connaissais déjà le réseau de transports en commun.
- C'est une occasion rêvée de raviver le débat Paris/Province qui fait rage, déjà que personne ne sait placer Besançon sur une carte.
L'idée générale de l'application est de créer une carte interactive de la ville de Besançon sur laquelle
les utilisateurs peuvent voir les horaires des lignes de bus et de tramway, et de pouvoir laisser des commentaires.
On peut différencier 3 grands types de transports en commun à Besançon :
- Les lignes de bus urbaines (lignes L3 à L6 et 7 à 12), qui desservent la ville de Besançon et les zones commerciales limitrophes.
- Les lignes de bus suburbaines (lignes 20 à 89 + lignes "Diabolo" scolaires), qui desservent les communes de l'agglomération de Besançon. Mais au vu de ce qui se trouve dans cette agglomération, ce dispositif est plus à considérer comme un réseau de bus de campagne.
- Les lignes de tramway (T1 et T2) qui desservent les principaux pôles de la ville de Besançon.
Il a été fait le choix de ne considérer que les lignes de bus urbaines et les lignes de tramway, le réseau de Besançon 
proposant une quantité de lignes de bus suburbaines et scolaires trop importante pour être gérée dans le cadre de ce projet, et
les informations mises à disposition sont plus restreintes. On se serait retrouvé avec une carte trop chargée et peu lisible.

### Fonctionnalités

Comme précisé dans le sujet, l'application permet de consulter les horaires des lignes de bus et de tramway de Besançon au travers d'une carte interactive.
Cette interactivité se fait au travers de 3 fonctionnalités principales :
- La possibilité de sélectionner une ligne de bus ou de tramway sur la carte pour afficher la direction.
- La possibilité de sélectionner un arrêt de bus ou de tramway sur la carte pour afficher les lignes qui y passent ainsi que les horaires des prochains passages.
- La possibilité d'obtenir des précisions sur les horaires ainsi que d'obtenir / poster un commentaire sur un arrêt de bus ou de tramway.
Au premier coup d'oeil, on peut voir les différentes lignes de bus et de tramway qui passent dans la ville de Besançon, et les arrêts de bus et de tramway.
Ces arrêts sont représentés par des marqueurs de couleur différentes qui s'accumulent en fonction du nombre de lignes qui y passent, selon le code couleur défini par l'exploitant.

### Choix techniques

Dans la page documentant l'open data de Ginko (https://data.ginko.voyage), on peut voir que les requêtes à l'API nécessitent une clé API personnelle.
Il a donc fallu commencer par contacter Ginko pour obtenir cette fameuse clé. Ils ont pris ma demande et mes motivations sur le choix de ce sujet avec beaucoup d'humour.
Une fois la clé obtenue, il a fallu se pencher sur la documentation de l'API pour comprendre comment elle fonctionne.
Une partie des données étaients disponibles au format GTFS, en plus de l'utilisation de l'API REST. En effet, ces deux formats étaient complémentaires.
Il a également été fait le choix d'utiliser des Servlets pour la partie serveur, plutôt que de faire un serveur en Go. Il a donc fallu mettre en place un serveur Tomcat.
Les données aux format GTFS ont dû être converties en base de données relationnelle pour pouvoir être utilisées par le serveur.

La base de données relationnelle a été implémentée avec MariaDB, et le serveur Tomcat a été configuré pour utiliser cette base de données aux moyens du connecteur JDBC.
C'est une technologie que je connaissais déjà d'où le choix du relationnel.

## Architecture

### Serveur

Il a été fait le choix de structurer le serveur comme étant une API REST, avec des routes pour chaque fonctionnalité, abordant ainsi une
approche ressource, c'est à dire que aux requêtes de fournir le contexte, et que les appels à l'API sont auto-suffisants.
Les routes sont les suivantes :
- `/api/lines` : Pour les lignes de bus et de tramway.
- `/api/stops` : Pour les arrêts de bus et de tramway.
- `/api/vehicules` (non-utilisé) : Pour les véhicules.

Chaque route est associée à une Servlet, qui va se charger de récupérer les données nécessaires à la requête, et de les renvoyer au format JSON.
Les différents appels sont plus ou moins identiques pour toutes les routes, à savoir :
- `getAll`
- `getById`
- `putComment`
- `getComments`

Les appels spécifiques sont ceux pour récupérer le tracée et les arrêts composant une ligne de bus ou de tramway, et ceux pour récupérer les lignes passant par un arrêt de bus ou de tramway, ainsi
que la récupération des informations concernant le prochain passage d'un bus ou d'un tramway à un arrêt donné.

A l'origine, j'avais eu l'idée de compléter ces informations avec les données de localisation des véhicules ainsi que les données d'affluence,
mais ce sont des informations soumises à des droits d'accès, et je n'ai pas pu les obtenir. Ceci explique aussi pourquoi la partie véhicule a été implémentée mais n'est pas utilisée.

Ce que l'on peut retenir de ce choix d'implémentation, c'est que le rôle du serveur est de servir une API au client, dont l'objectif
est d'encapsuler et de formater la plupart des appels à l'API de Ginko.
Nous avons également quelques appels à la base de données pour récupérer les commentaires et les informations sur les lignes et les arrêts.


### Client

Le client est monopage, et l'interface comprend une carte Leaflet prenant l'intégralité de l'écran, avec apparition éventuelle
de menus latéraux pour afficher des informations supplémentaires. La carte est initialisée avec un fond de carte OpenStreetMap.
Puis les données sont récupérées au travers de l'API REST du serveur, et affichées sur la carte.

Ce procédé, notamment celui pour récupérer les données des arrêts, est relativement long, et peut être optimisé.
Ceci s'explique par le grand volume de données, qui plus est, en temps réel. Il faudrait donc mettre en cache les données récupérées.

Il a été fait le choix de ne pas utiliser de framework tel que React, comme cela avait été suggéré dans l'énoncé, d'une part pour ne pas
alourdir le projet, mais également parce que ce n'est pas un framework que je connais. Il a donc été fait le choix de faire le code HTML, CSS, et JavaScript "à la main".

Voici à présent quelques captures d'écran de l'application :

### Interface globale de l'application :
![Interface globale de l'application](WebApp/report_data/interface.png)

### Détails d'un arrêt de bus :

![Détails d'un arrêt de bus](WebApp/report_data/stop.png)

### Commentaires :
![Commentaires](WebApp/report_data/commentaire.png)

